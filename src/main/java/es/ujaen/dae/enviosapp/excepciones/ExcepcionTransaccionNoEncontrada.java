/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.enviosapp.excepciones;

/**
 *
 * @author jmixe
 */
public class ExcepcionTransaccionNoEncontrada extends Exception{
    public ExcepcionTransaccionNoEncontrada(String msg)
    {
        super(msg);
    }
}
