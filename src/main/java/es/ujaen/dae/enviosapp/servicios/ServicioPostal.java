/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.enviosapp.servicios;

import es.ujaen.dae.enviosapp.entidades.Envio;
import es.ujaen.dae.enviosapp.entidades.PuntoControl.CentroLogistico;
import es.ujaen.dae.enviosapp.entidades.PuntoControl.Oficina;
import es.ujaen.dae.enviosapp.entidades.PuntoControl.PuntoControl;
import es.ujaen.dae.enviosapp.entidades.Transaccion;
import es.ujaen.dae.enviosapp.excepciones.ExcepcionCargaJSON;
import es.ujaen.dae.enviosapp.excepciones.ExcepcionEnvioNoEncontrado;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

/**
 * Clase principal de la aplicación
 */
@Service
public class ServicioPostal {

    /**
     * Mapa con la lista de puntos de control
     */
    Map<Integer, PuntoControl> puntosControl;
    /**
     * Mapa con la lista de envíos ordenada por el localizador
     */
    Map<String, Envio> envios;

    public ServicioPostal() throws ExcepcionCargaJSON {
        puntosControl = new TreeMap<>();
        envios = new TreeMap<>();

        try {
            cargarCentrosLogisticos();
        } catch (ParseException | IOException ex) {
            throw new ExcepcionCargaJSON("No se ha podido cargar el fichero JSON");
        }
    }

    // Cargar los centros logísticos y puntos de control desde el archivo JSON
    private void cargarCentrosLogisticos() throws FileNotFoundException, ParseException, IOException {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(new FileReader("src/main/java/es/ujaen/dae/enviosapp/util/redujapack.json", Charset.forName("UTF-8")));
        ArrayList<CentroLogistico> centrosAux = new ArrayList();
        ArrayList<ArrayList<Integer>> redConex = new ArrayList();

        for (int i = 1; obj.containsKey(Integer.toString(i)); i++) {
            JSONObject puntoLogistico = (JSONObject) obj.get(Integer.toString(i));
            String nombre = (String) puntoLogistico.get("nombre");
            String localizacion = (String) puntoLogistico.get("localización");
            int id = i;
            ArrayList<Oficina> prov = new ArrayList();
            ArrayList<CentroLogistico> conex = new ArrayList();
            ArrayList<Integer> indicesConex = new ArrayList();

            CentroLogistico cenLog = new CentroLogistico(id, nombre, localizacion, prov, conex);
            puntosControl.put(id, cenLog);

            JSONArray provincias = (JSONArray) puntoLogistico.get("provincias");
            int idOfi = 0;
            for (Object p : provincias) {
                idOfi++;
                int idOficina = Integer.valueOf(cenLog.getIdPC() + "" + idOfi);
                Oficina ofi = new Oficina(idOficina, (String) p, cenLog);
                cenLog.getProvincias().add(ofi);
                puntosControl.put(ofi.getIdPC(), ofi);
            }

            JSONArray conexiones = (JSONArray) puntoLogistico.get("conexiones");
            for (Object c : conexiones) {
                indicesConex.add(((Long) c).intValue());
            }
            redConex.add(indicesConex);
            centrosAux.add(cenLog);
        }
        for (int i = 0; i < centrosAux.size(); i++) {
            for (int j = 0; j < redConex.get(i).size(); j++) {
                centrosAux.get(i).getConexiones().add(centrosAux.get(redConex.get(i).get(j)-1));
            }
        }
    }

    // Crear un envio y asignar el localizador
    public Envio crearEnvio(String ciudadOrigen, String ciudadDestino, String remitente, String destinatario,
            float volumen, float peso) {

        // Generar localizador de 10 dígitos aleatorio y no usado previamente
        String localizador;
        do {
            localizador = Long.toString((new Random().nextLong() % 9000000000L) + 1000000000L);
        } while (envios.containsKey(localizador));

        //Hay que calcular la ruta y con ello el precio
        ArrayList<Transaccion> ruta = crearRuta(ciudadOrigen, ciudadDestino);

        float precio = calcularPrecio(ruta, peso, volumen);

        //Creamos el envio
        Envio envio = new Envio(ruta, localizador, ciudadOrigen, ciudadDestino, remitente, destinatario, volumen, peso, precio);

        //Lo almacenamos en el mapa
        envios.put(envio.getLocalizador(), envio);

        return envio;
    }

    public ArrayList<Transaccion> seguimientoEnvio(String localizador) throws ExcepcionEnvioNoEncontrado {
        if(envios.containsKey(localizador)){
            throw new ExcepcionEnvioNoEncontrado("No se ha encontrado un envio con ese localizador");
        }
        return envios.get(localizador).seguimientoEnvio();
    }
    
    public String estadoEnvio(String localizador) throws ExcepcionEnvioNoEncontrado{
        if(envios.containsKey(localizador)){
            throw new ExcepcionEnvioNoEncontrado("No se ha encontrado un envio con ese localizador");
        }
        return envios.get(localizador).getEstado().toString();
    }

    public void notificarSalida(String localizador, int IdPuntoControl) throws ExcepcionEnvioNoEncontrado {
        if(envios.containsKey(localizador)){
            throw new ExcepcionEnvioNoEncontrado("No se ha encontrado un envio con ese localizador");
        }
        envios.get(localizador).notificarSalida();
    }

    public void notificarEntrada(String localizador, int IdPuntoControl) throws ExcepcionEnvioNoEncontrado {
        if(envios.containsKey(localizador)){
            throw new ExcepcionEnvioNoEncontrado("No se ha encontrado un envio con ese localizador");
        }
        envios.get(localizador).notificarEntrada();
    }

    public void notificarEntrega(String localizador) throws ExcepcionEnvioNoEncontrado {
        if(envios.containsKey(localizador)){
            throw new ExcepcionEnvioNoEncontrado("No se ha encontrado un envio con ese localizador");
        }
        envios.get(localizador).paqueteEntregado(LocalDateTime.now());
    }

    public static ArrayList<CentroLogistico> shortestPathBFS(CentroLogistico startNode, CentroLogistico nodeToBeFound) {
        boolean shortestPathFound = false;
        Queue<CentroLogistico> queue = new LinkedList();
        Set<CentroLogistico> visitedNodes = new HashSet();
        ArrayList<CentroLogistico> shortestPath = new ArrayList();
        queue.add(startNode);
        shortestPath.add(startNode);
        Map<CentroLogistico, CentroLogistico> parentNodes = new HashMap();

        while (!queue.isEmpty()) {
            CentroLogistico nextNode = queue.peek();
            shortestPathFound = (nextNode == nodeToBeFound) ? true : false;
            if (shortestPathFound) {
                break;
            }
            visitedNodes.add(nextNode);
            System.out.println(queue);
            CentroLogistico unvisitedNode = null;
            if (nextNode.getConexiones() != null) {
                for (int i = 0; i < nextNode.getConexiones().size(); i++) {
                    if (!visitedNodes.contains(nextNode.getConexiones().get(i))) {
                        unvisitedNode = nextNode.getConexiones().get(i);
                    }
                }
            }
            if (unvisitedNode != null) {
                queue.add(unvisitedNode);
                visitedNodes.add(unvisitedNode);
                parentNodes.put(unvisitedNode, nextNode);
                shortestPathFound = (unvisitedNode == nodeToBeFound) ? true : false;
                if (shortestPathFound) {
                    shortestPath = new ArrayList();
                    CentroLogistico node = nodeToBeFound;
                    while (node != null) {
                        shortestPath.add(node);
                        node = parentNodes.get(node);
                    }
                    Collections.reverse(shortestPath);
                }
            } else {
                queue.poll();
            }
        }
        return shortestPath;
    }

    //Calculo de la ruta del envio
    public ArrayList<Transaccion> crearRuta(String ciudadOrigen, String ciudadDestino) {
        ArrayList<Transaccion> ruta = new ArrayList<>();
        CentroLogistico CLOrigen, CLDestino;
        int idOrigen = 0, idDestino = 0;
        for (Map.Entry<Integer, PuntoControl> ob : puntosControl.entrySet()) {
            if (puntosControl.get(ob.getKey()).getNombrePC().equals(ciudadOrigen)) {
                idOrigen = ob.getKey();
            }
            if (puntosControl.get(ob.getKey()).getNombrePC().equals(ciudadDestino)) {
                idDestino = ob.getKey();
            }
        }

        if (puntosControl.get(idOrigen) instanceof CentroLogistico) {
            CLOrigen = (CentroLogistico) puntosControl.get(idOrigen);
        } else {
            CLOrigen = ((Oficina) puntosControl.get(idOrigen)).getCentroAsociado();
        }
        if (puntosControl.get(idDestino) instanceof CentroLogistico) {
            CLDestino = (CentroLogistico) puntosControl.get(idDestino);
        } else {
            CLDestino = ((Oficina) puntosControl.get(idDestino)).getCentroAsociado();
        }

        //Caso 1: Misma provincia
        if (ciudadOrigen.equals(ciudadDestino)) {
            ruta.add(new Transaccion(puntosControl.get(idOrigen), LocalDateTime.now()));
        } else {
            //Caso 2: Distinta provincia, mismo centro logístico
            if (CLOrigen.equals(CLDestino)) {
                ruta.add(new Transaccion(puntosControl.get(idOrigen), LocalDateTime.now()));
                ruta.add(new Transaccion(CLOrigen));
                ruta.add(new Transaccion(puntosControl.get(idDestino)));
            } //Caso 3: Distinta provincia, distinto centro logístico
            else {
                ArrayList<CentroLogistico> camino = shortestPathBFS(CLOrigen, CLDestino);
                ruta.add(new Transaccion(puntosControl.get(idOrigen), LocalDateTime.now()));
                for (int i = 0; i < camino.size(); i++) {
                    ruta.add(new Transaccion(camino.get(i)));
                }
                ruta.add(new Transaccion(puntosControl.get(idDestino)));
            }
        }

        return ruta;
    }

    //Calculo del precio segun la ruta
    private float calcularPrecio(ArrayList<Transaccion> ruta, float peso, float tamaño) {

        //Sumamos uno más al contar el camión
        float precio = (peso * tamaño * (ruta.size() + 2)) / 1000;

        return precio;
    }
}
