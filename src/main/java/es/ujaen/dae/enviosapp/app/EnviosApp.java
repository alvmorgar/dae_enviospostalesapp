package es.ujaen.dae.enviosapp.app;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import es.ujaen.dae.enviosapp.excepciones.ExcepcionCargaJSON;
import es.ujaen.dae.enviosapp.servicios.ServicioPostal;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author alvmo
 */
@SpringBootApplication(scanBasePackages = "es.ujaen.dae.enviosapp.servicios")
public class EnviosApp {
    
    ServicioPostal ServicioPostal() throws ExcepcionCargaJSON{
        ServicioPostal serviciopostal = new ServicioPostal();
        // Cargar centros logisticos serviciopostal.LeerJSON();
        return serviciopostal;
    }
    public static void main(String[] args) throws Exception {
        // Creación de servidor
        SpringApplication servidor = new SpringApplication(EnviosApp.class);
        ApplicationContext context = servidor.run(args);
    }
}
