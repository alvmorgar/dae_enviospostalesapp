/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.enviosapp.entidades.PuntoControl;

import java.util.ArrayList;

/**
 *
 * @author alvmo
 */
public class CentroLogistico extends PuntoControl{
    
    String localizacion;
    ArrayList<Oficina> provincias;
    ArrayList<CentroLogistico> conexiones;

    public CentroLogistico(int idPC, String nombrePC, String localizacion, ArrayList<Oficina> prov, ArrayList<CentroLogistico> conex) {
        this.idPC = idPC;
        this.nombrePC = nombrePC;
        this.localizacion = localizacion;
        this.provincias = prov;
        this.conexiones = conex;
    }
    
    public String getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    public ArrayList<Oficina> getProvincias() {
        return provincias;
    }

    public void setProvincias(ArrayList<Oficina> provincias) {
        this.provincias = provincias;
    }

    public ArrayList<CentroLogistico> getConexiones() {
        return conexiones;
    }

    public void setConexiones(ArrayList<CentroLogistico> conexiones) {
        this.conexiones = conexiones;
    }
    
    
}
