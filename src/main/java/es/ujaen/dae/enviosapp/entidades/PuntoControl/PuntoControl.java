/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.enviosapp.entidades.PuntoControl;

/**
 *
 * @author alvmo
 */
public class PuntoControl {

    int idPC;
    String nombrePC;

    public PuntoControl() {

    }

    public PuntoControl(int idPC, String nombrePC) {
        this.idPC = idPC;
        this.nombrePC = nombrePC;
    }

    public int getIdPC() {
        return idPC;
    }

    public void setIdPC(int id) {
        this.idPC = id;
    }

    public String getNombrePC() {
        return nombrePC;
    }

    public void setNombrePC(String nombrePC) {
        this.nombrePC = nombrePC;
    }

}
