/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.enviosapp.entidades.PuntoControl;

/**
 *
 * @author alvmo
 */
public class Oficina extends PuntoControl{
    CentroLogistico centroAsociado;

    public CentroLogistico getCentroAsociado() {
        return centroAsociado;
    }

    public void setCentroAsociado(CentroLogistico centroAsociado) {
        this.centroAsociado = centroAsociado;
    }

    public Oficina(int idPC, String nombre, CentroLogistico centroAsociado) {
        this.idPC = idPC;
        this.nombrePC = nombre;
        this.centroAsociado = centroAsociado;
    }
    
    
}
