/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.enviosapp.entidades;

import java.time.LocalDateTime;
import es.ujaen.dae.enviosapp.excepciones.ExcepcionTransaccionNoEncontrada;
import es.ujaen.dae.enviosapp.util.Estado;
import java.util.ArrayList;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.validation.annotation.Validated;

/**
 *
 * Entidad Envio
 */
@Validated
public class Envio {

    /**
     * Lista de transacciones del envio
     */
    ArrayList<Transaccion> ruta;

    @NotNull
    private String localizador;
    @NotBlank
    private String ciudadOrigen;
    private String ciudadDestino;
    private String remitente;
    private String destinatario;
    private float volumen;
    private float peso;
    private float precio;
    private LocalDateTime fechaEntrega;
    private Estado estado;

    public Envio(ArrayList<Transaccion> ruta, String localizador, String ciudadOrigen,
            String ciudadDestino, String remitente, String destinatario,
            float tamaño, float peso, float precio) {

        this.ruta = ruta;
        this.localizador = localizador;
        this.ciudadOrigen = ciudadOrigen;
        this.ciudadDestino = ciudadDestino;
        this.remitente = remitente;
        this.destinatario = destinatario;
        this.volumen = tamaño;
        this.peso = peso;
        this.precio = precio;
        this.fechaEntrega = null;
        this.estado = Estado.EN_TRANSITO;
    }

    /**
     * Listar puntos de control por los que ha pasado el paquete
     *
     * @return Lista de registros con los puntos de control visitados hasta el
     * momento
     */
    public ArrayList<Transaccion> seguimientoEnvio() {
        return (ArrayList<Transaccion>) ruta.stream().filter(r
                -> r.getEntrada() != null)
                .sorted()
                .collect(Collectors.toList());
    }

    public void notificarEntrada(){

        for (int i = 0; i < ruta.size(); i++) {
            Transaccion tr = ruta.get(i);
            if (tr.getSalida() == null) {
                tr.setEntrada(LocalDateTime.now());
            }
        }
    }

    public void notificarSalida() {

        for (int i = 0; i < ruta.size(); i++) {
            Transaccion tr = ruta.get(i);
            if (tr.getEntrada() == null) {
                tr.setSalida(LocalDateTime.now());
                if (ruta.get(ruta.size() - 1).equals(tr)) {
                    setEstado(Estado.EN_REPARTO);
                }
            }
        }
    }

    /**
     * @param fechaEntrega the fechaEntrega to set
     */
    public void paqueteEntregado(LocalDateTime fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
        this.setEstado(Estado.ENTREGADO);
    }

    public Transaccion buscarTransaccion(int idPuntoControl) throws ExcepcionTransaccionNoEncontrada {
        for (Transaccion trans : ruta) {
            if (trans.getPuntoControl().getIdPC() == idPuntoControl) {
                return trans;
            }
        }
        throw new ExcepcionTransaccionNoEncontrada("Transaccion no encontrada en el envio");
    }

    /**
     * @return the localizador
     */
    public String getLocalizador() {
        return localizador;
    }

    /**
     * @param localizador the localizador to set
     */
    public void setLocalizador(String localizador) {
        this.localizador = localizador;
    }

    /**
     * @return the ciudadOrigen
     */
    public String getCiudadOrigen() {
        return ciudadOrigen;
    }

    /**
     * @param ciudadOrigen the ciudadOrigen to set
     */
    public void setCiudadOrigen(String ciudadOrigen) {
        this.ciudadOrigen = ciudadOrigen;
    }

    /**
     * @return the ciudadDestino
     */
    public String getCiudadDestino() {
        return ciudadDestino;
    }

    /**
     * @param ciudadDestino the ciudadDestino to set
     */
    public void setCiudadDestino(String ciudadDestino) {
        this.ciudadDestino = ciudadDestino;
    }

    /**
     * @return the remitente
     */
    public String getRemitente() {
        return remitente;
    }

    /**
     * @param remitente the remitente to set
     */
    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

    /**
     * @return the destinatario
     */
    public String getDestinatario() {
        return destinatario;
    }

    /**
     * @param destinatario the destinatario to set
     */
    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    /**
     * @return the volumen
     */
    public float getVolumen() {
        return volumen;
    }

    /**
     * @param volumen the tamaño to set
     */
    public void setVolumen(float volumen) {
        this.volumen = volumen;
    }

    /**
     * @return the peso
     */
    public float getPeso() {
        return peso;
    }

    /**
     * @param peso the peso to set
     */
    public void setPeso(float peso) {
        this.peso = peso;
    }

    /**
     * @return the precio
     */
    public float getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }

    /**
     * @return the fechaEntrega
     */
    public LocalDateTime getFechaEntrega() {
        return fechaEntrega;
    }

    /**
     * @return the estado
     */
    public Estado getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}
