/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.enviosapp.entidades;

import es.ujaen.dae.enviosapp.entidades.PuntoControl.PuntoControl;
import java.time.LocalDateTime;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import org.springframework.validation.annotation.Validated;

/**
 * Clase que representa el paso del envío por un punto de control
 */
public class Transaccion implements Comparable{
    
    /**Fecha de registro */
    @PastOrPresent
    private LocalDateTime entrada;
    
    /**Fecha de salida */
    @FutureOrPresent
    private LocalDateTime salida;
    
    /**Punto de control por el que ha pasado */
    @NotNull
    private PuntoControl puntoControl;

    public Transaccion(PuntoControl puntoControl, LocalDateTime entrada, LocalDateTime salida)
    {
        this.puntoControl = puntoControl;
        this.entrada = entrada;
        this.salida = salida;
    }
    
    public Transaccion(PuntoControl puntoControl, LocalDateTime entrada)
    {
        this.puntoControl = puntoControl;
        this.entrada = entrada;
        this.salida = null;
    }
    public Transaccion(PuntoControl puntoControl)
    {
        this.puntoControl = puntoControl;
        this.entrada = null;
        this.salida = null;
    }

    public LocalDateTime getEntrada() {
        return entrada;
    }

    public void setEntrada(LocalDateTime entrada) {
        this.entrada = entrada;
    }

    public LocalDateTime getSalida() {
        return salida;
    }

    public void setSalida(LocalDateTime salida) {
        this.salida = salida;
    }

    public PuntoControl getPuntoControl() {
        return puntoControl;
    }

    public void setPuntoControl(PuntoControl puntoControl) {
        this.puntoControl = puntoControl;
    }

    /**
     * Clase de comparación entre transacciones por fecha de entrada
     * @param o Objeto a comparar
     * @return Resultado de la comparacion
     */
    @Override
    public int compareTo(Object o) {
        Transaccion otra = (Transaccion) o;
        
        return this.getEntrada().compareTo(otra.entrada);
    }   
}
