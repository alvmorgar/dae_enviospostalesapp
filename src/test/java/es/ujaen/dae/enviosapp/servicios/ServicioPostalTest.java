/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.enviosapp.servicios;

import es.ujaen.dae.enviosapp.app.EnviosApp;
import es.ujaen.dae.enviosapp.entidades.Envio;
import es.ujaen.dae.enviosapp.entidades.Transaccion;
import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

/**
 *
 * @author alvmo
 */
//@Disabled
@SpringBootTest(classes = {EnviosApp.class})
public class ServicioPostalTest {
    
    @Autowired
    ServicioPostal servicioPostal;
    
    @Test
    public void ServicioPostalTest(){
        Assertions.assertNotNull(servicioPostal);
    }
    
    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCrearEnvio(){
        String ciudadOrigen = "Jaén", ciudadDestino = "Granada";
        String remitente = "Orestes Melanina", destinatario = "Rubén Cigarro";
        float volumen = 10, peso = 5;
        Envio envioPrueba = servicioPostal.crearEnvio(ciudadOrigen, ciudadDestino, remitente, destinatario, volumen, peso);
        
        //Si envioPrueba no es nulo, significa que efectivamente se ha creado puesto
        //que devuelve el envío.
        Assertions.assertNotNull(envioPrueba);
        Assertions.assertEquals(0.25, envioPrueba.getPrecio());
        
    }
    
    @Test
    //@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCrearRuta(){
        String ciudadOrigen1 = "Granada";
        
        String ciudadOrigen2 = "Valencia";
        String ciudadDestino2 = "Alicante";
        
        String ciudadOrigen3 = "Cáceres";
        String ciudadDestino3 = "Madrid";
        
        ArrayList<Transaccion> ruta1 = servicioPostal.crearRuta(ciudadOrigen1, ciudadOrigen1);
        ArrayList<Transaccion> ruta2 = servicioPostal.crearRuta(ciudadOrigen2, ciudadDestino2);
        ArrayList<Transaccion> ruta3 = servicioPostal.crearRuta(ciudadOrigen3, ciudadDestino3);
        
        Assertions.assertTrue(ruta1.size() == 1);
        Assertions.assertEquals(ciudadOrigen1, ruta1.get(0).getPuntoControl().getNombrePC());
        
        Assertions.assertTrue(ruta2.size() == 3);
        Assertions.assertEquals(ciudadOrigen2, ruta2.get(0).getPuntoControl().getNombrePC());
        Assertions.assertEquals("CL Levante", ruta2.get(1).getPuntoControl().getNombrePC());
        Assertions.assertEquals(ciudadDestino2, ruta2.get(2).getPuntoControl().getNombrePC());
        
        Assertions.assertTrue(ruta3.size() == 5);
        Assertions.assertEquals(ciudadOrigen3, ruta3.get(0).getPuntoControl().getNombrePC());
        Assertions.assertEquals("CL Andalucía-Extremadura", ruta3.get(1).getPuntoControl().getNombrePC());
        Assertions.assertEquals("CL Castilla La Mancha", ruta3.get(2).getPuntoControl().getNombrePC());
        Assertions.assertEquals("CL Madrid", ruta3.get(3).getPuntoControl().getNombrePC());
        Assertions.assertEquals(ciudadDestino3, ruta3.get(4).getPuntoControl().getNombrePC()); 
    }
}
