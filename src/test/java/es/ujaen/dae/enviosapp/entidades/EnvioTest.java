/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.enviosapp.entidades;

import es.ujaen.dae.enviosapp.entidades.PuntoControl.PuntoControl;
import es.ujaen.dae.enviosapp.excepciones.ExcepcionTransaccionNoEncontrada;
import java.util.ArrayList;
import org.assertj.core.api.Assertions;
import static org.assertj.core.api.Assertions.fail;
import org.junit.Test;

/**
 *
 * @author alvmo
 */
public class EnvioTest {

    public EnvioTest() {
    }

    @Test
    public void testNotificarSalida() {

        ArrayList<Transaccion> ruta = new ArrayList<>();
        PuntoControl pc = new PuntoControl(0, "Jaén");
        Transaccion tr = new Transaccion(pc);
        ruta.add(tr);

        Envio envio = new Envio(
                ruta,
                "asdf",
                "Jaén",
                "Granada",
                "Juan Chica Chica",
                "Alberto Torres Torres",
                15,
                8,
                12);

        envio.notificarSalida();

        try {
            Assertions.assertThat(envio.buscarTransaccion(0).getSalida()).isNotNull();
        } catch (ExcepcionTransaccionNoEncontrada e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testNotificarEntrada() {

        ArrayList<Transaccion> ruta = new ArrayList<>();
        PuntoControl pc = new PuntoControl(0, "Jaén");
        Transaccion tr = new Transaccion(pc);
        ruta.add(tr);

        Envio envio = new Envio(
                ruta,
                "asdf",
                "Jaén",
                "Granada",
                "Juan Chica Chica",
                "Alberto Torres Torres",
                15,
                8,
                12);

        envio.notificarEntrada();

        try {
            Assertions.assertThat(envio.buscarTransaccion(0).getEntrada()).isNotNull();
        } catch (ExcepcionTransaccionNoEncontrada e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void buscarTransaccion() {

        ArrayList<Transaccion> ruta = new ArrayList<>();
        PuntoControl pc1 = new PuntoControl(0, "Jaén");
        Transaccion tr1 = new Transaccion(pc1);
        PuntoControl pc2 = new PuntoControl(1, "Centro logistico");
        Transaccion tr2 = new Transaccion(pc2);
        PuntoControl pc3 = new PuntoControl(2, "Granada");
        Transaccion tr3 = new Transaccion(pc3);
        ruta.add(tr1);
        ruta.add(tr2);
        ruta.add(tr3);

        Envio envio = new Envio(
                ruta,
                "asdf",
                "Jaén",
                "Granada",
                "Juan Chica Chica",
                "Alberto Torres Torres",
                15,
                8,
                12);
        try {
            Assertions.assertThat(ruta.get(0))
                    .isEqualToComparingFieldByField(envio.buscarTransaccion(0));
            Assertions.assertThat(ruta.get(1))
                    .isEqualToComparingFieldByField(envio.buscarTransaccion(1));
            Assertions.assertThat(ruta.get(2))
                    .isEqualToComparingFieldByField(envio.buscarTransaccion(2));
        } catch (ExcepcionTransaccionNoEncontrada e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSeguimiento() {
        ArrayList<Transaccion> ruta = new ArrayList<>();
        PuntoControl pc1 = new PuntoControl(0, "Jaén");
        Transaccion tr1 = new Transaccion(pc1);
        PuntoControl pc2 = new PuntoControl(1, "Centro logistico");
        Transaccion tr2 = new Transaccion(pc2);
        PuntoControl pc3 = new PuntoControl(2, "Granada");
        Transaccion tr3 = new Transaccion(pc3);
        ruta.add(tr1);
        ruta.add(tr2);
        ruta.add(tr3);
        Envio envio = new Envio(
                ruta,
                "asdf",
                "Jaén",
                "Granada",
                "Juan Chica Chica",
                "Alberto Torres Torres",
                15,
                8,
                12);

        envio.notificarEntrada();
        envio.notificarSalida();
        envio.notificarEntrada();
        envio.notificarSalida();
        envio.notificarEntrada();
        envio.notificarSalida();

        ArrayList<Transaccion> listaSeguimiento = envio.seguimientoEnvio();

        try {
            Assertions.assertThat(envio.buscarTransaccion(0)).isEqualTo(listaSeguimiento.get(0));
            Assertions.assertThat(envio.buscarTransaccion(1)).isEqualTo(listaSeguimiento.get(1));
            Assertions.assertThat(envio.buscarTransaccion(2)).isEqualTo(listaSeguimiento.get(2));
        } catch (ExcepcionTransaccionNoEncontrada e) {
            fail(e.getMessage());
        }
    }

}
